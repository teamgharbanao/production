from django.conf import settings
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.contrib import admin
from django.core.mail import send_mail
from django.shortcuts import render
from designers.models import Designers,Rawpass
from django.core.files.storage import FileSystemStorage
from designers.forms import SendMail
from django.core.mail import send_mail
from django.conf import settings
from gbstore.models import *
from rest_framework import routers
from apis import views
from rest_framework.urlpatterns import format_suffix_patterns
from datetime import datetime,date
from gbstore.forms import ProductAdd,PlywoodAddForm,LaminatesAddForm,AdhesiveAdd
import random
import string
from django.contrib.auth.hashers import make_password
from merchant.forms import MerchantAddForm,SGForm,merchantSearch
from merchant.models import *
from designers.models import Designers



def Mail(request):
    email=""
    if request.method == "POST":
        MyRegisterForm = SendMail(request.POST)
        if MyRegisterForm.is_valid():
            Send_To=MyRegisterForm.cleaned_data['Send_To']
            dbuser = Designers.objects.filter(email=Send_To)
            if dbuser:
                user=dbuser[0]
                randpass=''.join(random.choice(string.ascii_uppercase+string.digits) for _ in range(8))
                dbuser[0].password=make_password(randpass)
                dbuser[0].designerID='GBIDX0{0}'.format(Designers.objects.count())
                rawpass=Rawpass(rawpass=randpass,designer=dbuser[0])
                rawpass.save()
                dbuser[0].save()
                contact_message =  'Your DesignerId is {0} and password is  {1} '.format(user.designerID,randpass)
                from_email = settings.EMAIL_HOST_USER
                to_email = []
                to_email.append(Send_To)
                send_mail('Registration Succesfull', contact_message, from_email, to_email, fail_silently=False)



        else:
            MyRegisterForm = SendMail(request.GET)

    return render(request, 'designers/SendMails.html',{})

def product_add(request):
    errors=""

    if request.method=='POST':
        myform=ProductAdd(request.POST,request.FILES)

        if myform.is_valid():
            p=Products()
            count=Products.objects.count()+1
            p.gbcode='GBPRO0{0}'.format(count)
            if myform.cleaned_data['comp_code']:
                p.comp_code = myform.cleaned_data['comp_code']
            else:
                p.comp_code=p.gbcode

            p.name=myform.cleaned_data['name']
            p.company=myform.cleaned_data['company']
            p.address=myform.cleaned_data['address']
            if myform.cleaned_data['description']:
              p.description=myform.cleaned_data['description']
            if myform.cleaned_data['colour']:
                p.colour=myform.cleaned_data['colour']
            if myform.cleaned_data['length']:
                p.length=myform.cleaned_data['length']
            if myform.cleaned_data['height']:
                p.height=myform.cleaned_data['height']
            if myform.cleaned_data['radius']:
                p.radius=myform.cleaned_data['radius']
            p.image1=myform.cleaned_data['image1']
            if myform.cleaned_data['image2']:
                p.image2=myform.cleaned_data['image2']
            p.original_price=myform.cleaned_data['original_price']
            p.cost_price=myform.cleaned_data['cost_price']
            p.selling_price=myform.cleaned_data['selling_price']
            p.profit=((p.selling_price-p.cost_price)/p.cost_price)*100
            if myform.cleaned_data['star']:
                p.star=True
            else:
                p.star=False
            p.date=datetime.now()
            p.save()
            context = {'errors': errors}
            return render(request, 'gbstore/product_add.html', context=context)


        else:
            print(myform.errors)
            errors="Not Inserted"
    context = {'errors': errors}
    return render(request,'gbstore/product_add.html',context=context)

def product_search(request):


    products=list(Products.objects.filter(star=True).order_by('-date')[:10])
    nonstar=Products.objects.filter(star=False).order_by('date')[:20]
    for x in nonstar:
        products.append(x)
    random.shuffle(products)
    new=Products.objects.filter(new=True).order_by('date')[:10]
    if request.method=='POST':
        products=[]
        products=list(Products.objects.filter(name__contains=request.POST['search']).order_by('date'))
        products.extend(list(Products.objects.filter(company__contains=request.POST['search']).order_by('date')))
        products.extend(list(Products.objects.filter(description__contains=request.POST['search']).order_by('date')))
        new=[]
    context={
        'products':products,
        'new':new,
    }
    return render(request, 'gbstore/product_search.html', context=context)

def product_update(request):
    errors = ""
    context = {'errors': errors}
    return render(request, 'gbstore/product_add.html', context=context)


def plywood_add(request):
    if request.method=='POST':

        myform=PlywoodAddForm(request.POST,request.FILES)
        print(request.POST.getlist('vendor'))
        if myform.is_valid():
            ply=Plywood()
            ply.brand = myform.cleaned_data['brand']
            ply.dimention = myform.cleaned_data['dimention']
            ply.Thickness = myform.cleaned_data['Thickness']
            ply.series = myform.cleaned_data['series']
            if myform.cleaned_data['img1']:
                ply.img1 = myform.cleaned_data['img1']
            if myform.cleaned_data['img2']:
                ply.img2 = myform.cleaned_data['img2']
            if myform.cleaned_data['compcode']:
                ply.compcode = myform.cleaned_data['compcode']
            if myform.cleaned_data['description']:
                ply.description = myform.cleaned_data['description']
            ply.original_price = myform.cleaned_data['original_price']
            ply.cost_price = myform.cleaned_data['cost_price']
            ply.selling_price = myform.cleaned_data['selling_price']
            ply.date=datetime.now()
            code=Plywood.objects.count()+1
            ply.gbcode='GBPLY0{0}'.format(code)
            gbcode=ply.gbcode
            ply.save()
            for x in request.POST.getlist('vendor'):

                if Merchants.objects.filter(merchantID=x):
                    merch=Mercentdise()
                    merch.type="Plywood"
                    merch.merchant=Merchants.objects.get(merchantID=x)
                    merch.gbcode=gbcode
                    merch.save()
            br=Brand.objects.filter(name=ply.brand,typ='Plywood')
            if not br:
                br1=Brand()
                br1.name=ply.brand
                br1.typ='Plywood'
                br1.save()
            dim = Dimention.objects.filter(name=ply.dimention, typ='Plywood')
            if not dim:
                dim1 = Dimention()
                dim1.name = ply.dimention
                dim1.typ = 'Plywood'
                dim1.save()
            thik=Thickness.objects.filter(name=ply.Thickness,typ='Plywood')
            if not thik:
                thik1 = Thickness()
                thik1.name = ply.Thickness
                thik1.typ = 'Plywood'
                thik1.save()
    vendor = list(Merchants.objects.all())

    content={

        'merchants':vendor
    }
    return render(request,'gbstore/plywood_add.html',content)


def laminates_add(request):
    if request.method=='POST':
        myform=LaminatesAddForm(request.POST,request.FILES)
        if myform.is_valid():
            ply=Laminates()
            ply.brand = myform.cleaned_data['brand']
            ply.dimention = myform.cleaned_data['dimention']
            ply.Thickness = myform.cleaned_data['Thickness']
            ply.series = myform.cleaned_data['series']
            if myform.cleaned_data['img1']:
                ply.img1 = myform.cleaned_data['img1']
            if myform.cleaned_data['img2']:
                ply.img2 = myform.cleaned_data['img2']
            if myform.cleaned_data['compcode']:
                ply.compcode = myform.cleaned_data['compcode']
            if myform.cleaned_data['description']:
                ply.description = myform.cleaned_data['description']
            ply.original_price = myform.cleaned_data['original_price']
            ply.cost_price = myform.cleaned_data['cost_price']
            ply.selling_price = myform.cleaned_data['selling_price']
            ply.date=datetime.now()
            code=Laminates.objects.count()+1
            ply.gbcode='GBLAM0{0}'.format(code)
            ply.dim = myform.cleaned_data['dim']
            ply.finish = myform.cleaned_data['finish']
            ply.save()
            for x in request.POST.getlist('vendor'):

                if Merchants.objects.filter(merchantID=x):
                    merch=Mercentdise()
                    merch.type="Laminates"
                    merch.merchant=Merchants.objects.get(merchantID=x)
                    merch.gbcode=ply.gbcode
                    merch.save()
            br = Brand.objects.filter(name=ply.brand, typ='Laminates')
            if not br:
                br1 = Brand()
                br1.name = ply.brand
                br1.typ = 'Laminates'
                br1.save()
            dim = Dimention.objects.filter(name=ply.dimention, typ='Laminates')
            if not dim:
                dim1 = Dimention()
                dim1.name = ply.dimention
                dim1.typ = 'Laminates'
                dim1.save()
            thik = Thickness.objects.filter(name=ply.Thickness, typ='Laminates')
            if not thik:
                thik1 = Thickness()
                thik1.name = ply.Thickness
                thik1.typ = 'Laminates'
                thik1.save()
            di=Dim.objects.filter(name=ply.dim,typ='Laminates')
            if not di:
                di1=Dim()
                di1.name=ply.dim
                di1.typ='Laminates'
                di1.save()
    vendor = list(Merchants.objects.all())

    content = {

        'merchants': vendor
    }


    return render(request,'gbstore/laminate_add.html',content)



def adhesives_add(request):
    if request.method=='POST':
        myform=AdhesiveAdd(request.POST,request.FILES)
        if myform.is_valid():
            ply=Adhesives()
            ply.brand = myform.cleaned_data['brand']
            ply.quantity = myform.cleaned_data['quantity']
            ply.appication = myform.cleaned_data['application']
            ply.series = myform.cleaned_data['series']
            if myform.cleaned_data['img1']:
                ply.img1 = myform.cleaned_data['img1']
            if myform.cleaned_data['img2']:
                ply.img2 = myform.cleaned_data['img2']
            if myform.cleaned_data['compcode']:
                ply.compcode = myform.cleaned_data['compcode']
            if myform.cleaned_data['description']:
                ply.description = myform.cleaned_data['description']
            ply.original_price = myform.cleaned_data['original_price']
            ply.cost_price = myform.cleaned_data['cost_price']
            ply.selling_price = myform.cleaned_data['selling_price']
            ply.date=datetime.now()
            code=Adhesives.objects.count()+1
            ply.gbcode='GBADH0{0}'.format(code)
            ply.save()
            for x in request.POST.getlist('vendor'):

                if Merchants.objects.filter(merchantID=x):
                    merch=Mercentdise()
                    merch.type="Adhesives"
                    merch.merchant=Merchants.objects.get(merchantID=x)
                    merch.gbcode=ply.gbcode
                    merch.save()
            br = Brand.objects.filter(name=ply.brand, typ='Adhesives')
            if not br:
                br1 = Brand()
                br1.name = ply.brand
                br1.typ = 'Adhesives'
                br1.save()
            dim = Quantity.objects.filter(name=ply.quantity, typ='Adhesives')
            if not dim:
                dim1 = Quantity()
                dim1.name = ply.quantity
                dim1.typ = 'Adhesives'
                dim1.save()
            thik = Appication.objects.filter(name=ply.appication, typ='Adhesives')
            if not thik:
                thik1 = Appication()
                thik1.name = ply.appication
                thik1.typ = 'Adhesives'
                thik1.save()
        print(myform.errors)

    vendor = list(Merchants.objects.all())

    content = {

        'merchants': vendor
    }
    return render(request,'gbstore/adhesive_add.html',content)


def bills(request):
    my_bills=Bill.objects.all()
    bill=[]
    for b in my_bills:
        single_bill=[]
        single_bill.append(b)
        entities=Entity.objects.filter(bill=b)
        single_bill.append(entities)
        bill.append(single_bill)
    context={
        'bills':bill
    }

    return render(request,'gbstore/bills.html',context)

def searchbydate(request):
    my_bills = Bill.objects.filter(date=request.GET['dated'])
    bill = []
    for b in my_bills:
        single_bill = []
        single_bill.append(b)
        entities = Entity.objects.filter(bill=b)
        single_bill.append(entities)
        bill.append(single_bill)
    context = {
        'bills': bill
    }

    return render(request, 'gbstore/bills.html', context)

def searchbydesigner(request):
    my_bills = []
    designers=Designers.objects.filter(name__contains=request.GET['name'])
    for d in designers:
        dbills=Bill.objects.filter(designer=d)
        my_bills.extend(dbills)
    bill = []
    for b in my_bills:
        single_bill = []
        single_bill.append(b)
        entities = Entity.objects.filter(bill=b)
        single_bill.append(entities)
        bill.append(single_bill)
    context = {
        'bills': bill
    }

    return render(request, 'gbstore/bills.html', context)

def searchbybillid(request):
    my_bills = Bill.objects.filter(billid=request.GET['billid'])
    bill = []
    for b in my_bills:
        single_bill = []
        single_bill.append(b)
        entities = Entity.objects.filter(bill=b)
        single_bill.append(entities)
        bill.append(single_bill)
    context = {
        'bills': bill
    }

    return render(request, 'gbstore/bills.html', context)


def merchant_add(request):
    content={}
    merchants = Merchants.objects.all()
    content['merchants'] = merchants
    if request.method=='GET':
        myform=merchantSearch(request.GET)

        if myform.is_valid():
            searchfield=myform.cleaned_data['searchfield']
            byname=list(Merchants.objects.filter(name__contains=searchfield))
            byname.extend(list(Merchants.objects.filter(address__contains=searchfield)))
            byname.extend(list(Merchants.objects.filter(city__contains=searchfield)))
            byname=set(byname)
            content={
                'merchants':byname
            }
            return render(request, 'merchant/merchant_add.html', content)
    if request.method=='POST':
        myform=MerchantAddForm(request.POST)
        if myform.is_valid():
            m=Merchants()
            m.name=myform.cleaned_data['name']
            m.phone = myform.cleaned_data['phone']
            m.city= myform.cleaned_data['city']
            m.address = myform.cleaned_data['address']
            count=Merchants.objects.count()+1
            m.merchantID="GBMer00"+str(count)
            m.save()
            print(m)


    return render(request,'merchant/merchant_add.html',content)

def showroom_add(request):
    if request.method=='POST':
        myform=SGForm(request.POST)
        if myform.is_valid():
            obj=Showroom()
            obj.merchant=Merchants.objects.get(merchantID=myform.cleaned_data['merchantID'])
            obj.address=myform.cleaned_data['address']
            obj.phone=myform.cleaned_data['phone']
            obj.save()
        else:
            print(myform.errors)
    content = {}
    merchants = Merchants.objects.all()
    content['merchants'] = merchants
    return render(request, 'merchant/merchant_add.html', content)

def godown_add(request):
    if request.method=='POST':
        myform=SGForm(request.POST)
        if myform.is_valid():
            obj=Godown()
            obj.merchant=Merchants.objects.get(merchantID=myform.cleaned_data['merchantID'])
            obj.address=myform.cleaned_data['address']
            obj.phone=myform.cleaned_data['phone']
            obj.save()
        else:
            print(myform.errors)
    content = {}
    merchants = Merchants.objects.all()
    content['merchants'] = merchants
    return render(request, 'merchant/merchant_add.html', content)

def merchant_view(request):
    id=request.GET['merchantID']
    m=Merchants.objects.get(merchantID=id)
    s=Showroom.objects.filter(merchant=m)
    g=Godown.objects.filter(merchant=m)
    products=[]
    mer=Mercentdise.objects.filter(merchant=m)
    for x in mer:
        if Plywood.objects.filter(gbcode=x.gbcode):
            products.append(["Plywood",Plywood.objects.filter(gbcode=x.gbcode)])
        elif Laminates.objects.filter(gbcode=x.gbcode):
                products.append(["Laminates", Laminates.objects.filter(gbcode=x.gbcode)])
        elif Adhesives.objects.filter(gbcode=x.gbcode):
            products.append(["Adhesives", Adhesives.objects.filter(gbcode=x.gbcode)])
    print(products)
    content={
        'merchant':m,
        'showrooms':s,
        'godowns':g,
        'products':products
    }

    return render(request, 'merchant/merchant_view.html', content)

def protovendor(request):
    if request.method=='GET':
        myform=merchantSearch(request.GET)
        if myform.is_valid():
            searchfield= myform.cleaned_data['searchfield']
            pcodes=list(Mercentdise.objects.filter(gbcode__contains=searchfield))
            merchants=[]
            for m in pcodes:
                merchants.append(m.merchant)
            content={
                'm':merchants
            }
            print(merchants)
            return render(request, 'gbstore/protovendor.html', content)
        else:
            print(myform.errors)
    return render(request, 'gbstore/protovendor.html')




urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^designers/',include('designers.urls')),
    url(r'^',include('customers.urls')),
    url(r'^designers/', include('designers.urls')),
    url(r'^store/',include('gbstore.urls')),
    url(r'^mail/',  admin.site.admin_view(Mail), name='Mail'),
    url(r'^product_add/',admin.site.admin_view(product_add),name='Product'),
    url(r'^product_search',admin.site.admin_view(product_search),name='ProductSearch'),
    url(r'^product_update',admin.site.admin_view(product_update),name='ProductUpdate'),
    url(r'^plywood',admin.site.admin_view(plywood_add),name='Plywoodadd'),
    url(r'^laminates',admin.site.admin_view(laminates_add),name='Laminatesadd'),
    url(r'^adhesives',admin.site.admin_view(adhesives_add),name='Adhesivesadd'),
    url(r'^bills',admin.site.admin_view(bills),name='bills'),
    url(r'^searchbydate',admin.site.admin_view(searchbydate),name='searchbydate'),
    url(r'^searchbydesigner',admin.site.admin_view(searchbydesigner),name='searchbydesigner'),#searchbybillid
    url(r'^searchbybillid',admin.site.admin_view(searchbybillid),name='searchbybillid'),
    url(r'^andro/',include('apis.urls')),
    url(r'^merchant/',include('merchant.urls')),
    url(r'^merchant_add',admin.site.admin_view(merchant_add),name='merchant_add'),
    url(r'^showroom_add',admin.site.admin_view(showroom_add),name='showroom_add'),
    url(r'^godown_add',admin.site.admin_view(godown_add),name='godown_add'),
    url(r'^merchant_view',admin.site.admin_view(merchant_view),name='view_merchant'),
    url(r'^protovendor', admin.site.admin_view(protovendor), name='producttovendor'),

]

urlpatterns+=static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
urlpatterns+=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
