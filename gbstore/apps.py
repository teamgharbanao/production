from django.apps import AppConfig


class GbstoreConfig(AppConfig):
    name = 'gbstore'
