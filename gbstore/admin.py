from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Products)
admin.site.register(Plywood)
admin.site.register(Laminates)
admin.site.register(Brand)
admin.site.register(Dimention)
admin.site.register(Dim)
admin.site.register(Thickness)
admin.site.register(Adhesives)
admin.site.register(Quantity)
admin.site.register(Bill)
admin.site.register(Entity)
admin.site.register(Appication)
admin.site.register(Mercentdise)