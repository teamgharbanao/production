from django.db import models
from datetime import datetime,date
from designers.models import *
from merchant.models import Merchants

# Create your models here.
class Products(models.Model):
    gbcode=models.CharField(max_length=15)
    comp_code=models.CharField(null=True,blank=True,max_length=30)
    name=models.TextField(max_length=100)
    company=models.TextField(max_length=100)
    address=models.TextField(max_length=300)
    description=models.TextField(max_length=300,blank=True,null=True)
    colour=models.CharField(max_length=50,blank=True,null=True)
    length=models.IntegerField(default=0,null=True,blank=True)
    height=models.IntegerField(default=0,null=True,blank=True)
    breadth=models.IntegerField(default=0,null=True,blank=True)
    radius=models.IntegerField(default=0,null=True,blank=True)
    image1=models.ImageField(null=True,blank=True)
    image2=models.ImageField(null=True,blank=True)
    original_price=models.FloatField()
    cost_price=models.FloatField()
    selling_price=models.FloatField()
    profit=models.FloatField()
    star=models.BooleanField(default=False)
    new=models.BooleanField(default=True)
    date=models.DateField()


    def __str__(self):
        r=self.name+" "+self.gbcode+" "+self.comp_code
        return r




class Plywood(models.Model):
    gbcode = models.CharField(max_length=25)
    date = models.DateField()
    brand=models.CharField(max_length=50)
    dimention=models.CharField(max_length=10)
    Thickness=models.IntegerField(default=0)
    series=models.CharField(max_length=20)
    img1=models.ImageField(blank=True,null=True)
    img2=models.ImageField(blank=True,null=True)
    compcode=models.CharField(max_length=25,null=True,blank=True)
    description=models.TextField(max_length=500,null=True,blank=True)
    original_price = models.FloatField()
    cost_price = models.FloatField()
    selling_price = models.FloatField()

    def __str__(self):
        return self.gbcode

class Mercentdise(models.Model):
    type=models.CharField(max_length=30)
    merchant=models.ForeignKey(Merchants,on_delete=models.CASCADE,null=True,blank=True)
    gbcode=models.CharField(max_length=25)


class Laminates(models.Model):
    gbcode = models.CharField(max_length=25)
    original_price = models.FloatField()
    cost_price = models.FloatField()
    selling_price = models.FloatField()
    date = models.DateField()
    brand = models.CharField(max_length=50)
    dim=models.CharField(max_length=5)
    finish = models.CharField(max_length=50)
    Thickness = models.IntegerField(default=0)
    series = models.CharField(max_length=20)
    img1 = models.ImageField(blank=True, null=True)
    img2 = models.ImageField(blank=True, null=True)
    compcode = models.CharField(max_length=25, null=True, blank=True)
    description = models.TextField(max_length=500, null=True, blank=True)




class Adhesives(models.Model):
    gbcode = models.CharField(max_length=25)
    original_price = models.FloatField()
    cost_price = models.FloatField()
    selling_price = models.FloatField()
    date = models.DateField()
    brand = models.CharField(max_length=50)
    series = models.CharField(max_length=20)
    img1 = models.ImageField(blank=True, null=True)
    img2 = models.ImageField(blank=True, null=True)
    compcode = models.CharField(max_length=25, null=True, blank=True)
    description = models.TextField(max_length=500, null=True, blank=True)
    quantity=models.CharField(max_length=12)
    appication=models.CharField(max_length=60)


class Appication(models.Model):
    typ=models.CharField(max_length=50)
    name=models.CharField(max_length=50)

    def __str__(self):
        r = self.name
        return r

class Quantity(models.Model):
    typ=models.CharField(max_length=50)
    name=models.CharField(max_length=50)

    def __str__(self):
        r = self.name
        return r



class Brand(models.Model):
    typ=models.CharField(max_length=50)
    name=models.CharField(max_length=50)

    def __str__(self):
        r = self.name
        return r


class Dimention(models.Model):
    typ=models.CharField(max_length=50)
    name=models.CharField(max_length=50)

    def __str__(self):
        r = self.name
        return r


class Thickness(models.Model):
    typ=models.CharField(max_length=50)
    name = models.CharField(max_length=50)

    def __str__(self):
        r = self.name
        return r


class Dim(models.Model):
    typ=models.CharField(max_length=50)
    name=models.CharField(max_length=50)

    def __str__(self):
        r = self.name
        return r


class Bill(models.Model):
    designer = models.ForeignKey(Designers, on_delete=models.CASCADE)
    billid=models.CharField(max_length=30)
    checked=models.BooleanField(default=False,blank=True)
    total=models.FloatField(null=True,blank=True)
    date=models.DateField()

class Entity(models.Model):
    bill=models.ForeignKey(Bill,on_delete=models.CASCADE)
    parent=models.CharField(max_length=50)
    gbcode=models.CharField(max_length=50)
    quantity=models.CharField(max_length=50)
    contact=models.CharField(blank=True,null=True,default="Harlalka",max_length=100)
    price=models.FloatField()
    total=models.FloatField(default=0)
