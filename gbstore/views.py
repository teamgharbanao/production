from django.shortcuts import render
from .models import *
from django.views.decorators.cache import cache_control
from django.conf import settings
from django.core.files.storage import FileSystemStorage #for saving images in /media
from .forms import SearchForm,LoginForm,OrderForm,Cart,DeleteProductForm
from designers.models import Designers
from django.contrib.auth.hashers import make_password, check_password
from django.views.decorators.cache import cache_control
import random
import string
from datetime import datetime,date

# Create your views here.
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def login(request):
    if request.session.has_key('designerID'):
        return render(request, 'gbstore/home.html')
    if request.method=='POST':
        content={}
        myform=LoginForm(request.POST)
        if myform.is_valid():
            designerID=myform.cleaned_data['designerID']
            password=myform.cleaned_data['password']
            dbuser = Designers.objects.filter(designerID=designerID)
            if dbuser and check_password(password, dbuser[0].password):
                user = dbuser[0]
                # profilepic = user.profilepic.url
                request.session['designerID'] = designerID
                if(request.session.has_key('cart') and len(dict(request.session['cart']))>0):
                    content={
                        'count':len(dict(request.session['cart']))
                    }
                return render(request, 'gbstore/home.html',content)
    return render(request,'gbstore/login.html')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def ref(request):
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    if request.method == 'GET':

        myform = SearchForm(request.GET)

        if myform.is_valid():
            if request.session.has_key('category') and not  request.session['category']==myform.cleaned_data['category']:
                try:
                    del request.session['state']
                except:
                    pass
            if myform.cleaned_data['category'] == 'All':
                pass
            if myform.cleaned_data['category'] == 'Plywood':
                if myform.cleaned_data['search']:
                    product = []
                    search_words=list(myform.cleaned_data['search'].split(" "))
                    i=0
                    for x in search_words:
                        product.extend(list(Plywood.objects.filter(series__contains=x)))
                        product.extend(list(Plywood.objects.filter(brand__contains=x)))
                        product.extend(list(Plywood.objects.filter(compcode__contains=x)))

                    product=list(set(product))
                    if  request.session.has_key('state'):
                        state=request.session['state']
                        refinedbrand=[]
                        for x in state['brand']:
                            refinedbrand.extend(list(Plywood.objects.filter(brand=x)))
                        if not state['brand']:
                            for x in list(Brand.objects.filter(typ='Plywood')):
                                refinedbrand.extend(list(Plywood.objects.filter(brand=x.name)))

                        refinedthik=[]

                        for x in state['thik']:
                            refinedthik.extend(list(Plywood.objects.filter(Thickness=x)))

                        if not state['thik']:
                                for x in list(Thickness.objects.filter(typ='Plywood')):
                                    refinedthik.extend(list(Plywood.objects.filter(Thickness=x.name)))


                        reffineddimentions=[]
                        for x in state['dimention']:

                            reffineddimentions.extend(list(Plywood.objects.filter(dimention=x)))
                        if not state['dimention']:
                            for x in list(Dimention.objects.filter(typ='Plywood')):

                                reffineddimentions.extend(list(Plywood.objects.filter(dimention=x.name)))



                        

                        product=list(set([val for val in product if val in refinedbrand and val in refinedthik and val in reffineddimentions]))

                        
                else:
                    product=Plywood.objects.all()
                brands = Brand.objects.filter(typ='Plywood')
                dimentions = Dimention.objects.filter(typ='Plywood')
                thik = Thickness.objects.filter(typ='Plywood')
                request.session['category']='Plywood'
                selected='Plywood'
                content = {
                    'product': product,
                    'brand': brands,
                    'dimentions': dimentions,
                    'thik': thik,
                    'selected':selected
                }
                if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                    content['count']=len(dict(request.session['cart']))
                return render(request, 'gbstore/searchresult.html', content)
            if myform.cleaned_data['category'] == 'Laminates':
                product = []
                search_words = list(myform.cleaned_data['search'].split(" "))
                for x in search_words:
                    product.extend(list(Laminates.objects.filter(series__contains=x)))
                    product.extend(list(Laminates.objects.filter(brand__contains=x)))
                    product.extend(list(Laminates.objects.filter(compcode__contains=x)))

                product = list(set(product))
                if request.session.has_key('state'):
                    state = request.session['state']
                    refinedbrand = []
                    refinedthik = []
                    refineddim = []


                    for x in state['brand']:
                        refinedbrand.extend(list(Laminates.objects.filter(brand=x)))
                    if not state['brand']:
                        for x in list(Brand.objects.filter(typ='Laminates')):
                            refinedbrand.extend(list(Laminates.objects.filter(brand=x.name)))

                    for x in state['thik']:
                        refinedthik.extend(list(Laminates.objects.filter(Thickness=x)))
                    if not state['thik']:
                        for x in list(Thickness.objects.filter(typ='Laminates')):
                            refinedthik.extend(list(Laminates.objects.filter(Thickness=x.name)))

                    for x in state['dim']:
                        refineddim.extend(list(Laminates.objects.filter(dim=x)))
                    if not state['dim']:
                        for x in list(Dim.objects.filter(typ='Laminates')):
                            refineddim.extend(list(Laminates.objects.filter(dim=x.name)))

                    product = list(set([val for val in product if val in refinedbrand and val in refinedthik and val in refineddim]))


                else:
                    product=Laminates.objects.all()
                brands = Brand.objects.filter(typ='Laminates')
                dimentions = Dimention.objects.filter(typ='Laminates')
                thik = Thickness.objects.filter(typ='Laminates')
                dim = Dim.objects.filter(typ='Laminates')
                request.session['category'] = 'Laminates'
                selected='Laminates'
                content = {
                    'product': product,
                    'brand': brands,
                    'dimentions': dimentions,
                    'thik': thik,
                    'dim': dim,
                    'selected':selected

                }

                if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                    content['count']=len(dict(request.session['cart']))
                return render(request, 'gbstore/searchresult.html', content)
            if myform.cleaned_data['category'] == 'Adhesives':
                product = []
                search_words = list(myform.cleaned_data['search'].split(" "))
                for x in search_words:
                    product.extend(list(Adhesives.objects.filter(series__contains=x)))
                    product.extend(list(Adhesives.objects.filter(brand__contains=x)))
                    product.extend(list(Adhesives.objects.filter(compcode__contains=x)))

                product = list(set(product))
                if request.session.has_key('state'):
                    state = request.session['state']
                    refinedbrand = []
                    refinedquantity = []
                    refinedapplication = []
                    state = request.session['state']
                    for x in state['brand']:
                        refinedbrand.extend(list(Adhesives.objects.filter(brand=x)))

                    if not state['brand']:
                        for x in list(Brand.objects.filter(typ='Adhesives')):
                            refinedbrand.extend(list(Adhesives.objects.filter(brand=x.name)))

                    for x in state['quantity']:
                        refinedquantity.extend(list(Adhesives.objects.filter(quantity=x)))
                    if not state['quantity']:
                        for x in list(Quantity.objects.filter(typ='Adhesives')):
                            refinedquantity.extend(list(Adhesives.objects.filter(quantity=x.name)))

                    for x in state['application']:
                        refinedapplication.extend(list(Adhesives.objects.filter(appication=x)))
                    if not state['application']:
                        for x in list(Appication.objects.filter(typ='Adhesives')):
                            refinedapplication.extend(list(Adhesives.objects.filter(appication=x.name)))


                    product = list( set([val for val in product if val in refinedbrand and val in refinedapplication and val in refinedquantity]))



                else:
                    product=Adhesives.objects.all()
                brands = Brand.objects.filter(typ='Adhesives')
                dimentions = Quantity.objects.filter(typ='Adhesives')
                thik = Appication.objects.filter(typ='Adhesives')
                selected = 'Adhesives'
                content = {
                    'product': product,
                    'brand': brands,
                    'quantity': dimentions,
                    'application': thik,
                    'selected':selected
                }
                request.session['category'] = 'Adhesives'
                if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                    content['count']=len(dict(request.session['cart']))

                return render(request, 'gbstore/searchresult.html', content)
    content={}
    return render(request,'gbstore/searchresult.html',content)

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def index(request):
    try:
        del request.session['state']
    except:
        pass
    content = {}
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
        content = {
            'count': len(dict(request.session['cart']))
        }
        return render(request, 'gbstore/home.html', content)
    return render(request, 'gbstore/home.html', content)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def filter(request):
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    content = {}
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    if request.session.has_key('category'):
        product=[]
        state={}
        selected=""
        if request.session['category']=='Plywood':
            selected='Plywood'
            brands=[]

            refinedbrand=[]
            for x in request.GET.getlist('brand'):
                brands.append(x)
                #product.extend(list(Plywood.objects.filter(brand=x)))
                refinedbrand.extend(list(Plywood.objects.filter(brand=x)))
            if not request.GET.getlist('brand'):
                for x in list(Brand.objects.filter(typ='Plywood')):
                    refinedbrand.extend(list(Plywood.objects.filter(brand=x.name)))
            state['brand']=brands
            dim=[]
            refinedthik=[]

            for x in request.GET.getlist('thik'):

                #product.extend(list(Plywood.objects.filter(Thickness=x)))
                refinedthik.extend(list(Plywood.objects.filter(Thickness=x)))
                dim.append(x)
            if not request.GET.getlist('thik'):
                    for x in list(Thickness.objects.filter(typ='Plywood')):

                        refinedthik.extend(list(Plywood.objects.filter(Thickness=x.name)))
            state['thik']=dim
            dimentions=[]
            reffineddimentions=[]
            for x in request.GET.getlist('dimentions'):
                dimentions.append(x)
                #product.extend(list(Plywood.objects.filter(dimention=x)))
                reffineddimentions.extend(list(Plywood.objects.filter(dimention=x)))
            if not request.GET.getlist('dimentions'):
                for x in list(Dimention.objects.filter(typ='Plywood')):

                    reffineddimentions.extend(list(Plywood.objects.filter(dimention=x.name)))

            state['dimention']=dimentions
            request.session['state']=state
            brands = Brand.objects.filter(typ='Plywood')
            #product = list(set(product))

            product=list(set([val for val in refinedbrand if val in refinedthik and val in reffineddimentions]))


            dimentions = Dimention.objects.filter(typ='Plywood')
            thik = Thickness.objects.filter(typ='Plywood')
            count=0
            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                count = len(dict(request.session['cart']))

            content = {
                'product': product,
                'brand': brands,
                'dimentions': dimentions,
                'thik': thik,
                'selected': selected

            }
            if not count==0:
                content['count']=count
            return render(request,'gbstore/searchresult.html',content)

        if request.session['category']=='Laminates':
            selected='Laminates'
            product = []
            state={}
            refinedbrand=[]
            refinedthik=[]
            refineddim=[]
            if request.session['category'] == 'Laminates':
                brands=[]
                for x in request.GET.getlist('brand'):
                    brands.append(x)
                    refinedbrand.extend(list(Laminates.objects.filter(brand=x)))
                if not request.GET.getlist('brand'):
                    for x in list(Brand.objects.filter(typ='Laminates')):
                        refinedbrand.extend(list(Laminates.objects.filter(brand=x.name)))
                state['brand']=brands
                thik=[]
                for x in request.GET.getlist('thik'):
                    thik.append(x)
                    refinedthik.extend(list(Laminates.objects.filter(Thickness=x)))
                if not request.GET.getlist('thik'):
                    for x in list(Thickness.objects.filter(typ='Laminates')):
                        refinedthik.extend(list(Laminates.objects.filter(Thickness=x.name)))
                state['thik']=thik
                dim=[]
                for x in request.GET.getlist('dim'):
                    dim.append(x)
                    refineddim.extend(list(Laminates.objects.filter(dim=x)))
                if not request.GET.getlist('dim'):
                    for x in list(Dim.objects.filter(typ='Laminates')):
                        refineddim.extend(list(Laminates.objects.filter(dim=x.name)))


                state['dim']=dim
            request.session['state']=state

            product=list(set([val for val in refinedbrand if val in refinedthik and val in refineddim]))
            brands = Brand.objects.filter(typ='Laminates')
            dimentions = Dimention.objects.filter(typ='Laminates')
            thik = Thickness.objects.filter(typ='Laminates')
            dim = Dim.objects.filter(typ='Laminates')
            count =0
            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                count = len(dict(request.session['cart']))

            content = {
                'product': product,
                'brand': brands,
                'dimentions': dimentions,
                'thik': thik,
                'selected': selected

            }
            if not count == 0:
                content['count'] = count
            return render(request, 'gbstore/searchresult.html', content)

        if request.session['category']=='Adhesives':
            selected='Adhesives'
            brands=[]
            refinedbrand=[]
            refinedquantity=[]
            refinedapplication=[]

            for x in request.GET.getlist('brand'):
                brands.append(x)
                refinedbrand.extend(list(Adhesives.objects.filter(brand=x)))

            if not request.GET.getlist('brand'):
                for x in list(Brand.objects.filter(typ='Adhesives')):
                    refinedbrand.extend(list(Adhesives.objects.filter(brand=x.name)))
            state['brand']=brands
            quantity=[]
            for x in request.GET.getlist('quantity'):
                quantity.append(x)
                refinedquantity.extend(list(Adhesives.objects.filter(quantity=x)))
            if not request.GET.getlist('quantity'):
                for x in list(Quantity.objects.filter(typ='Adhesives')):
                    refinedquantity.extend(list(Adhesives.objects.filter(quantity=x.name)))
            state['quantity']=quantity
            dimentions=[]
            for x in request.GET.getlist('application'):
                dimentions.append(x)
                refinedapplication.extend(list(Adhesives.objects.filter(appication=x)))
            if not request.GET.getlist('application'):
                for x in list(Appication.objects.filter(typ='Adhesives')):
                    refinedapplication.extend(list(Adhesives.objects.filter(appication=x.name)))
            state['application']=dimentions
            request.session['state']=state
            brands = Brand.objects.filter(typ='Adhesives')
            product = list(set([val for val in refinedbrand if val in refinedapplication and val in refinedquantity]))
            dimentions = Quantity.objects.filter(typ='Adhesives')
            thik = Appication.objects.filter(typ='Adhesives')
            count=0
            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                count = len(dict(request.session['cart']))

            content = {
                'product': product,
                'brand': brands,
                'dimentions': dimentions,
                'thik': thik,
                'selected': selected

            }
            if not count == 0:
                content['count'] = count
            return render(request, 'gbstore/searchresult.html', content)

        if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
            content = {
                'count': len(dict(request.session['cart']))
            }

        return render(request, 'gbstore/home.html', content)



def quantity(request):
    content = {}
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    productID=''
    if request.method=='GET':
        if not request.session.has_key('designerID'):
            return render(request, 'gbstore/login.html')
        myform=OrderForm(request.GET)
        if myform.is_valid():
            productID=myform.cleaned_data['productID']
            print(productID)
            if request.session['category']=='Plywood':
                if  Plywood.objects.get(gbcode=productID):
                    product=Plywood.objects.get(gbcode=productID)
                    content={
                        'p':product
                    }
                    return render(request, 'gbstore/quantity.html',content)
            if request.session['category']=='Laminates':
                if  Laminates.objects.get(gbcode=productID):
                    product=Laminates.objects.get(gbcode=productID)
                    content={
                        'p':product
                    }
                    return render(request, 'gbstore/quantity.html',content)
            if request.session['category']=='Adhesives':
                if  Adhesives.objects.get(gbcode=productID):
                    product=Adhesives.objects.get(gbcode=productID)
                    content={
                        'p':product
                    }
                    return render(request, 'gbstore/quantity.html',content)
            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                content = {
                    'count': len(dict(request.session['cart']))
                }
            return render(request, 'gbstore/home.html', content)

    if request.method=='POST':
        if not request.session.has_key('designerID'):
            return render(request, 'gbstore/login.html')

        cart = Cart(request.POST)
        content={}
        if cart.is_valid():
            qty = cart.cleaned_data['quantity']
            productID = cart.cleaned_data['productID']
            if  not request.session.has_key('cart'):
                request.session['cart']={}
                request.session['cart'][productID]=[qty,request.session['category']]

            else:
                c=request.session['cart']


                c[productID] = [qty,request.session['category']]

                request.session['cart']=c
    if request.session.has_key('state') and request.session.has_key('category'):
        if request.session['category']=='Plywood':
            state=request.session['state']
            refinedbrand=[]
            for x in state['brand']:
                refinedbrand.extend(list(Plywood.objects.filter(brand=x)))
            if not state['brand']:
                for x in list(Brand.objects.filter(typ='Plywood')):
                    refinedbrand.extend(list(Plywood.objects.filter(brand=x.name)))

            refinedthik=[]

            for x in state['thik']:
                refinedthik.extend(list(Plywood.objects.filter(Thickness=x)))

            if not state['thik']:
                    for x in list(Thickness.objects.filter(typ='Plywood')):
                        refinedthik.extend(list(Plywood.objects.filter(Thickness=x.name)))


            reffineddimentions=[]
            for x in state['dimention']:

                reffineddimentions.extend(list(Plywood.objects.filter(dimention=x)))
            if not state['dimention']:
                for x in list(Dimention.objects.filter(typ='Plywood')):

                    reffineddimentions.extend(list(Plywood.objects.filter(dimention=x.name)))



            brands = Brand.objects.filter(typ='Plywood')
            #product = list(set(product))

            product=list(set([val for val in refinedbrand if val in refinedthik and val in reffineddimentions]))


            dimentions = Dimention.objects.filter(typ='Plywood')
            thik = Thickness.objects.filter(typ='Plywood')
            request.session['category'] = 'Plywood'
            selected=''
            count=0
            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                count = len(dict(request.session['cart']))
                selected='Plywood'
            content = {
                'product': product,
                'brand': brands,
                'dimentions': dimentions,
                'thik': thik,
                'count':count,
                'selected': selected
            }
            del request.session['state']
            return render(request,'gbstore/searchresult.html',content)

        if request.session['category']=='Laminates':
            refinedbrand=[]
            refinedthik=[]
            refineddim=[]
            state=request.session['state']


            for x in state['brand']:
                refinedbrand.extend(list(Laminates.objects.filter(brand=x)))
            if not state['brand']:
                for x in list(Brand.objects.filter(typ='Laminates')):
                    refinedbrand.extend(list(Laminates.objects.filter(brand=x.name)))


            for x in state['thik']  :
                refinedthik.extend(list(Laminates.objects.filter(Thickness=x)))
            if not state['thik'] :
                for x in list(Thickness.objects.filter(typ='Laminates')):
                    refinedthik.extend(list(Laminates.objects.filter(Thickness=x.name)))


            for x in state['dim']:
                refineddim.extend(list(Laminates.objects.filter(dim=x)))
            if not state['dim']:
                for x in list(Dim.objects.filter(typ='Laminates')):
                    refineddim.extend(list(Laminates.objects.filter(dim=x.name)))

        product = list(set([val for val in refinedbrand if val in refinedthik and val in refineddim]))
        brands = Brand.objects.filter(typ='Laminates')
        dimentions = Dimention.objects.filter(typ='Laminates')
        thik = Thickness.objects.filter(typ='Laminates')
        dim = Dim.objects.filter(typ='Laminates')
        request.session['category'] = 'Laminates'
        count = 0
        selected = 'Laminates'
        if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
            count = len(dict(request.session['cart']))
        content = {
            'product': product,
            'brand': brands,
            'thik': thik,
            'dim': dim,
            'count':count,
            'selected': selected
        }
        return render(request, 'gbstore/searchresult.html', content)
    if request.session['category'] == 'Adhesives':

        refinedbrand = []
        refinedquantity = []
        refinedapplication = []
        state=request.session['state']
        for x in state['brand']:

            refinedbrand.extend(list(Adhesives.objects.filter(brand=x)))

        if not state['brand']:
            for x in list(Brand.objects.filter(typ='Adhesives')):
                refinedbrand.extend(list(Adhesives.objects.filter(brand=x.name)))


        for x in state['quantity'] :

            refinedquantity.extend(list(Adhesives.objects.filter(quantity=x)))
        if not state['quantity'] :
            for x in list(Quantity.objects.filter(typ='Adhesives')):
                refinedquantity.extend(list(Adhesives.objects.filter(quantity=x.name)))


        for x in state['application']:

            refinedapplication.extend(list(Adhesives.objects.filter(appication=x)))
        if not state['application']:
            for x in list(Appication.objects.filter(typ='Adhesives')):
                refinedapplication.extend(list(Adhesives.objects.filter(appication=x.name)))


        brands = Brand.objects.filter(typ='Adhesives')
        product = list(set([val for val in refinedbrand if val in refinedapplication and val in refinedquantity]))
        dimentions = Quantity.objects.filter(typ='Adhesives')
        thik = Appication.objects.filter(typ='Adhesives')
        count = 0
        selected = 'Adhesives'
        if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
            count = len(dict(request.session['cart']))

        content = {
            'product': product,
            'brand': brands,
            'quantity': dimentions,
            'application': thik,
            'count':count,
            'selected':selected
        }


        return render(request, 'gbstore/searchresult.html', content)
    elif request.session.has_key('category') and not request.session.has_key('state'):
        if request.session['category'] =='Adhesives':
            brands = Brand.objects.filter(typ='Adhesives')
            product = Adhesives.objects.all()
            dimentions = Quantity.objects.filter(typ='Adhesives')
            thik = Appication.objects.filter(typ='Adhesives')
            count=0
            selected='Adhesives'

            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                count = len(dict(request.session['cart']))
            content = {
                'product': product,
                'brand': brands,
                'quantity': dimentions,
                'application': thik,
                'count': count,
                'selected': selected
            }

            return render(request, 'gbstore/searchresult.html', content)
        elif request.session['category'] =='Laminates':
            product = Laminates.objects.all()
            brands = Brand.objects.filter(typ='Laminates')
            dimentions = Dimention.objects.filter(typ='Laminates')
            thik = Thickness.objects.filter(typ='Laminates')
            dim = Dim.objects.filter(typ='Laminates')
            request.session['category'] = 'Laminates'
            count = 0
            selected = 'Laminates'
            if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
                count = len(dict(request.session['cart']))
            content = {
                'product': product,
                'brand': brands,
                'thik': thik,
                'dim': dim,
                'count': count,
                'selected':selected
            }
            return render(request, 'gbstore/searchresult.html', content)
        elif request.session['category']=='Plywood':

            brands = Brand.objects.filter(typ='Plywood')
            # product = list(set(product))

            product = Plywood.objects.all()

            dimentions = Dimention.objects.filter(typ='Plywood')
            thik = Thickness.objects.filter(typ='Plywood')
            request.session['category'] = 'Plywood'
            count = 0
            selected = 'Plywood'
            if request.session.has_key('cart')  and len(dict(request.session['cart'])) > 0:
                count=len(dict(request.session['cart']))
            content = {
                'product': product,
                'brand': brands,
                'dimentions': dimentions,
                'thik': thik,
                'count':count,
                'selected':selected
            }
            return render(request, 'gbstore/searchresult.html', content)

    return render(request, 'gbstore/home.html', content)

@cache_control(no_cache=True,  no_store=True)
def mycart(request):
    if not request.session.has_key('designerID') or not request.session.has_key('cart'):
        if not request.session.has_key('cart'):
            return render(request, 'gbstore/cart.html')

        return render(request, 'gbstore/login.html')

    orders=[]
    cart=dict(request.session['cart'])
    total=0;
    print(request.session['cart'])
    for key,value in cart.items():
        if value[1]=='Laminates':
            x=Laminates.objects.filter(gbcode=key)
            orders.append([x[0],value[0]])
            total+=x[0].selling_price*float(value[0])

        elif value[1]=='Plywood':
            x = Plywood.objects.filter(gbcode=key)
            orders.append([x[0],value[0]])
            total += x[0].selling_price*float(value[0])

        elif value[1]=='Adhesives':
            x=Adhesives.objects.filter(gbcode=key)
            orders.append([x[0],value[0]])
            total += x[0].selling_price*float(value[0])

    content={
        'products':orders,
        'total':total
    }
    if(request.session.has_key('cart')):
        count=len(dict(request.session['cart']))
        content['count']=count

    return render(request,'gbstore/cart.html',content)





def deletecart(request):
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    content = {}
    del request.session['cart']
    if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
        content = {
            'count': len(dict(request.session['cart']))
        }
    return render(request, 'gbstore/cart.html', content)



def placeorder(request):
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    content = {}
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')
    if request.method=='POST':
        bill=Bill()
        id="GBBILL"+''.join(random.choice(string.ascii_uppercase+string.digits) for _ in range(4))+str(Bill.objects.count())
        bill.billid=id

        bill.designer=Designers.objects.get(designerID=request.session['designerID'])
        bill.date=datetime.now()
        bill.save()

        cart = dict(request.session['cart'])
        total = 0;
        print(request.session['cart'])
        for key, value in cart.items():
            if value[1] == 'Laminates':
                x = Laminates.objects.filter(gbcode=key)
                e=Entity()
                e.bill=bill
                e.parent='Laminates'
                e.quantity=value[0]
                print(x[0].selling_price)
                e.gbcode=key
                e.price= x[0].selling_price
                e.total=x[0].selling_price*float(value[0])
                e.save()
                total += x[0].selling_price*float(value[0])
            elif value[1] == 'Plywood':
                x = Plywood.objects.filter(gbcode=key)
                e = Entity()
                e.bill = bill
                e.parent = 'Plywood'
                print(x[0].selling_price)
                e.quantity = value[0]
                e.gbcode = key
                e.price = x[0].selling_price
                e.total = x[0].selling_price * float(value[0])
                e.save()
                total += x[0].selling_price*float(value[0])
            elif value[1] == 'Adhesives':
                x = Adhesives.objects.filter(gbcode=key)
                e = Entity()
                e.bill = bill
                e.parent = 'Adhesives'
                e.quantity = value[0]
                e.gbcode = key
                e.price = x[0].selling_price
                e.total = x[0].selling_price * float(value[0])
                e.save()
        b=Bill.objects.filter(billid=id)
        b.update(total=total)
        del request.session['cart']
        try:
            del request.session['state']

        except:
            pass
        try:
            del request.session['category']
        except:
            pass
    if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
        content = {
            'count': len(dict(request.session['cart']))
        }
    return render(request, 'gbstore/orderplaced.html', content)



def logout(request):
    try:
        del request.session['designerID']
        del request.session['cart']
    except:
        pass
    try:
        del request.session['state']
    except:
        pass
    try:
        del request.session['category']
    except:
        pass
    return render(request, 'designers/logout.html', {})

@cache_control(no_cache=True,  no_store=True)
def delete_product(request):
    if not request.session.has_key('designerID'):
        return render(request, 'gbstore/login.html')

    if request.method == 'POST':
        myform = DeleteProductForm(request.POST)
        orders = []
        total = 0

        if myform.is_valid():
            p = myform.cleaned_data['product']
            try:
                del request.session['cart'][p]

            except:
                pass
            if request.session.has_key('cart'):
                c = request.session['cart']
                if len(c) == 0:
                    del request.session['cart']
                    return render(request, 'gbstore/cart.html')
                for key, value in c.items():
                    if value[1] == 'Laminates':
                        x = Laminates.objects.filter(gbcode=key)
                        orders.append([x[0], value[0]])
                        total += x[0].selling_price * float(value[0])

                    elif value[1] == 'Plywood':
                        x = Plywood.objects.filter(gbcode=key)
                        orders.append([x[0], value[0]])
                        total += x[0].selling_price * float(value[0])

                    elif value[1] == 'Adhesives':
                        x = Adhesives.objects.filter(gbcode=key)
                        orders.append([x[0], value[0]])
                        total += x[0].selling_price * float(value[0])

                request.session['cart'] = c

                content = {
                    'products': orders,
                    'total': total
                }
                return render(request, 'gbstore/cart.html', content)

    content = {}
    if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):
        content = {
            'count': len(dict(request.session['cart']))
        }
    return render(request, 'gbstore/home.html', content)


def view_product(request):
    myform=OrderForm(request.GET)
    if myform.is_valid():
        productID=myform.cleaned_data['productID']
        if request.session['category'] == 'Plywood':
            if Plywood.objects.get(gbcode=productID):
                product = Plywood.objects.get(gbcode=productID)
                content = {
                    'p': product
                }
                return render(request, 'gbstore/view_product.html', content)
        if request.session['category'] == 'Laminates':
            if Laminates.objects.get(gbcode=productID):
                product = Laminates.objects.get(gbcode=productID)
                content = {
                    'p': product
                }
                return render(request, 'gbstore/view_product.html', content)
        if request.session['category'] == 'Adhesives':
            if Adhesives.objects.get(gbcode=productID):
                product = Adhesives.objects.get(gbcode=productID)
                content = {
                    'p': product
                }
                return render(request, 'gbstore/view_product.html', content)
    content = {}
    if (request.session.has_key('cart') and len(dict(request.session['cart'])) > 0):

        content = {
            'count': len(dict(request.session['cart']))
        }
    return render(request, 'gbstore/home.html', content)



def plyorder(request):
    return render(request,'gbstore/plyorder.html')

def lamorder(request):
    return render(request,'gbstore/lamorder.html')

def adhesiveorder(request):
    return render(request,'gbstore/adhesiveorder.html')

def paintorder(request):
    return render(request,'gbstore/paintorder.html')



def test(request):
    return render(request,'gbstore/test.html')

