from django import forms
from datetime import datetime,date

class LoginForm(forms.Form):
    designerID=forms.CharField()
    password=forms.CharField()


class ProductAdd(forms.Form):
    comp_code = forms.CharField(required=False)
    name = forms.CharField()
    company = forms.CharField()
    address = forms.CharField()
    description = forms.CharField(required=False)
    colour = forms.CharField(required=False)
    length = forms.IntegerField(required=False)
    height = forms.IntegerField(required=False)
    breadth = forms.IntegerField(required=False)
    radius = forms.IntegerField(required=False)
    image1 = forms.ImageField(required=False)
    image2 = forms.ImageField(required=False)
    original_price = forms.FloatField()
    cost_price = forms.FloatField()
    selling_price = forms.FloatField()
    star = forms.BooleanField(required=False)


class SearchForm(forms.Form):
    search=forms.CharField(required=False)


class PlywoodAddForm(forms.Form):
    brand = forms.CharField()
    dimention = forms.CharField()
    Thickness = forms.IntegerField()
    series = forms.CharField()
    img1 = forms.ImageField(required=False)
    img2 = forms.ImageField(required=False)
    compcode = forms.CharField(required=False)
    description = forms.CharField(required=False)
    original_price = forms.FloatField()
    cost_price = forms.FloatField()
    selling_price = forms.FloatField()

class LaminatesAddForm(forms.Form):
    brand = forms.CharField()
    dimention = forms.CharField()
    Thickness = forms.IntegerField()
    series = forms.CharField()
    img1 = forms.ImageField(required=False)
    img2 = forms.ImageField(required=False)
    compcode = forms.CharField(required=False)
    description = forms.CharField(required=False)
    original_price = forms.FloatField()
    cost_price = forms.FloatField()
    selling_price = forms.FloatField()
    dim = forms.CharField()
    finish = forms.CharField()

class AdhesiveAdd(forms.Form):
    brand = forms.CharField()
    quantity = forms.CharField()
    application = forms.CharField()
    series = forms.CharField()
    img1 = forms.ImageField(required=False)
    img2 = forms.ImageField(required=False)
    compcode = forms.CharField(required=False)
    description = forms.CharField(required=False)
    original_price = forms.FloatField()
    cost_price = forms.FloatField()
    selling_price = forms.FloatField()


class SearchForm(forms.Form):
    category=forms.CharField()
    search=forms.CharField(required=False)

class OrderForm(forms.Form):
    productID=forms.CharField()

class Cart(forms.Form):
    quantity=forms.CharField()
    productID=forms.CharField()

class DeleteProductForm(forms.Form):
    product=forms.CharField()
