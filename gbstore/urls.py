from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from . import views

app_name='gbstore'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ref$',views.ref,name='ref'),
    url(r'^filter$',views.filter,name='fil'),
    url(r'^login$',views.login,name='login'),
    url(r'^logout$',views.logout,name='logout'),
    url(r'quantity$',views.quantity,name='quantity'),
    url(r'^mycart$',views.mycart,name='mycart'),
    url(r'^deletecart',views.deletecart,name='deletecart'),
    url(r'^place_order',views.placeorder,name='place_order'),
    url(r'^delete_product$',views.delete_product,name='delete_product'),
    url(r'^view_product', views.view_product, name='view_product'),
	url(r'^test$',views.test,name='test'),
    url(r'^plyorder$',views.plyorder,name='plyorder'),
    url(r'^adhesiveorder$',views.adhesiveorder,name='adhesiveorder'),
    url(r'^paintorder$',views.paintorder,name='paintorder'),
    url(r'^lamorder$',views.lamorder,name='lamorder')

]