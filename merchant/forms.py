from django import forms

class MerchantAddForm(forms.Form):
    name = forms.CharField()
    phone = forms.CharField(required=False)
    city = forms.CharField(required=False)
    address = forms.CharField(required=False)

class SGForm(forms.Form):
    merchantID=forms.CharField()
    address=forms.CharField()
    phone=forms.CharField(required=False)


class merchantSearch(forms.Form):
    searchfield=forms.CharField()
