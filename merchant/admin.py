from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Merchants)
admin.site.register(Showroom)
admin.site.register(Godown)
