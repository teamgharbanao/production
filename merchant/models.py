from django.db import models


class Merchants(models.Model):
    merchantID = models.CharField(max_length=15)
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=10, blank=True, null=True)
    city = models.CharField(max_length=40, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name + " " + self.city


class Showroom(models.Model):
    merchant = models.ForeignKey(Merchants, on_delete=models.CASCADE)
    address = models.CharField(max_length=300, null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)


class Godown(models.Model):
    merchant = models.ForeignKey(Merchants, on_delete=models.CASCADE)
    address = models.CharField(max_length=300, null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)


from django.db import models

# Create your models here.
